import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Container, Grid, Paper, Typography, Box, TextField, Button, IconButton } from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { API_BASE_URL } from '../js/config';
import Escudo from "../assets/imagenes/login_back.png";

const styles = {
    container: {
        paddingTop: '5%',
        minHeight: '90vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    paper: {
        width: '100%',
        maxWidth: '100%',
    },
    typography: {
        color: "#64001D",
        fontWeight: "bold",
        padding: '5%',
    },
    gridItemLeft: {
        marginRight: '10px'
    },
    logoImage: {
        maxWidth: "100%",
        maxHeight: "100%",
        width: "auto"
    },
    form: {
        paddingLeft: '5%',
        paddingBottom: '5%',
    },
    button: {
        backgroundColor: "#64001D",
        color: "#FFFFFF",
        "&:hover": {
            backgroundColor: "#64001D",
        },
        marginTop: "10px",
        marginBottom: "5px",
    },
}

function LaboratorioForm() {
    const location = useLocation();
    const userToEdit = location.state?.userToEdit || {};
    const navigate = useNavigate();

    const [mision, setMision] = useState(userToEdit.mision || '');
    const [vision, setVision] = useState(userToEdit.vision || '');
    const [historia, setHistoria] = useState(userToEdit.historia || '');
    const [error, setError] = useState(false);

    const handleAgregar = async () => {
        if (!mision || !vision || !historia) {
            setError(true);
            return;
        }

        setError(false);
        const token = localStorage.getItem('token');
        try {
            const response = await axios.put(`${API_BASE_URL}registroLaboratorio/completar/${userToEdit.registro_id}`, {
                mision,
                vision,
                historia,
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Accept': 'application/json',
                }
            });
            if (response.status === 200) {
                navigate('/op_listaLab');
            }

            // Puedes añadir lógica adicional para manejar la respuesta del servidor, como navegación o mensajes de éxito/error.
        } catch (error) {
            console.error("Error al enviar el formulario:", error);
            // Puedes manejar errores específicos aquí si es necesario.
        }
    };

    const handleBack = () => {
        navigate(-1);
    };

    const handlecrudequipos = () => {
        navigate('/res_equipment', { state: { userToEdit } });
    };
    const handlecrudproyects = () => {
        navigate('/ManageProyects', { state: { userToEdit } });
    };
    const handlecrudoposted = () => {
        navigate('/res_publicaciones_public', { state: { userToEdit } });
    };
    const handlecrudservice = () => {
        navigate('/res_servicios_public', { state: { userToEdit } });
    };
    const handlecrudgalery = () => {
        navigate('/res_galeria_public', { state: { userToEdit } });
    };

    return (
        <Container mclassName='fondo'
        maxWidth="xl"
        sx={{
            paddingTop: '5%',
            minHeight: '50vh',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',

        }}>
            <Grid container spacing={4} justifyContent="center" alignItems="center">
                {/* Columna Izquierda (Imagen y Botones) */}
                <Grid item xs={4} sm={4} md={4} lg={3.5}>

                    <Box display="flex" justifyContent="space-between" alignItems="center" flexDirection="column" height="100%">
                        <img src={Escudo} alt="Logo-Sileii" style={{ maxWidth: "80%", height: "auto" }} />


                    </Box>

                </Grid>
                {/* Columna Derecha (Formulario) */}
                <Grid item xs={6} sm={6} md={6} lg={7}>
                    <Paper elevation={3} style={{ padding: '60px', width: '100%', }}>
                        {/* Botón de retroceso */}
                        <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
                            <IconButton
                                aria-label="back"
                                onClick={handleBack}
                                sx={{ color: '#64001D', fontWeight: 'bold', marginBottom: '10px' }}
                            >
                                <ArrowBackIcon />
                            </IconButton>
                            <Typography
                                variant="h5"
                                gutterBottom
                                align="left"
                                sx={{
                                    color: "#64001D",
                                    fontWeight: "bold",
                                    marginBottom: "20px"
                                  }}
                            >
                                Laboratorio
                            </Typography>
                        </Box>
                        <form style={styles.form}>
                            <Grid container spacing={1}>
                                <TextField
                                    label="id"
                                    variant="outlined"
                                    fullWidth
                                    margin="normal"
                                    sx={{
                                        visibility: 'hidden',
                                        position: 'absolute',
                                    }}
                                    name="id"
                                />
                                <Grid item xs={5.8} style={styles.gridItemLeft}>
                                    <TextField
                                        fullWidth
                                        name="Misión"
                                        label="Misión"
                                        variant="outlined"
                                        size='small'
                                        margin="dense"
                                        value={mision}
                                        onChange={(e) => setMision(e.target.value)}
                                    />
                                    <TextField
                                        fullWidth
                                        name="Visión"
                                        label="Visión"
                                        variant="outlined"
                                        size='small'
                                        margin="dense"
                                        value={vision}
                                        onChange={(e) => setVision(e.target.value)}
                                    />
                                    <TextField
                                        fullWidth
                                        name="Historia"
                                        label="Historia"
                                        variant="outlined"
                                        size='small'
                                        margin="dense"
                                        value={historia}
                                        onChange={(e) => setHistoria(e.target.value)}
                                    />

                                </Grid>
                            </Grid>
                            <Button
                                variant="contained"
                                onClick={handleAgregar}
                                style={{ marginTop: '20px', width: "260px" }}
                                sx={styles.button}
                            >
                                Agregar
                            </Button>
                        </form>
                    </Paper>
                </Grid>
                {/* Columna para los botones */}
                <Grid item xs={12} sm={12} md={6} lg={8}>
                    <Box display="flex" justifyContent="center" alignItems="center">
                        <Button variant="contained" color="primary" style={{ margin: "10px", fontSize: "15px", background: "#64001d" }} onClick={handlecrudequipos}>
                            Gestión Equipo
                        </Button>
                        <Button variant="contained" style={{ margin: "10px", fontSize: "15px", background: "#64001d" }} onClick={handlecrudproyects}>
                            Gestión Proyectos
                        </Button>
                        <Button variant="contained" style={{ margin: "10px", fontSize: "15px", background: "#64001d" }} onClick={handlecrudoposted}>
                            Gestión de Publicaciones
                        </Button>
                        <Button variant="contained" style={{ margin: "10px", fontSize: "15px", background: "#64001d" }} onClick={handlecrudservice}>
                            Gestión Servicios
                        </Button>
                        <Button variant="contained" style={{ margin: "10px", fontSize: "15px", background: "#64001d" }} onClick={handlecrudgalery}>
                            Galeria de fotos
                        </Button>
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}

export default LaboratorioForm;
