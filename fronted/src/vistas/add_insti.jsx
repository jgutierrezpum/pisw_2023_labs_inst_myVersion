import React from "react";
import { Container, Grid, Paper, Typography, Box, IconButton } from "@mui/material";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import InstiForm from "../componentes/InstiForm";
import Escudo from "../assets/imagenes/login_back.png";
import { useNavigate } from 'react-router-dom';
/* import Navigation from "../componentes/Nav"; */

function add_users() {
  const navigate = useNavigate();
  const handleBack = () => {
    navigate(-1);
  };
  return (
    <Container className='fondo'
    maxWidth="xl"
    sx={{
        paddingTop: '5%',
        minHeight: '50vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center', 
        
      }}>
      <Grid
        container
        spacing={4}
        justifyContent="center"
        alignItems="center"
      >
        {/* Columna Izquierda (Imagen) */}
        <Grid item xs={4} sm={4} md={4} lg={3.5}>
          <Box
            sx={{ display: 'flex', alignItems: 'flex-start', mb: 2 }}
          >
            <img
              src={Escudo}
              alt="Logo-Sileii"
              style={{ width: "50%" ,heigth: "auto" }}
            />
          </Box>
        </Grid>

        {/* Columna Derecha (Formulario) */}
        <Grid item xs={6} sm={6} md={6} lg={7}>
          <Paper
            elevation={3}
            style={{
              padding: '60px', width: '100%'
            }}
          >
            <Box sx={{ display: 'flex', alignItems: 'flex-start', mb: 2 }}>
              <IconButton
                aria-label="back"
                onClick={handleBack}
                sx={{ color: '#64001D', fontWeight: 'bold', marginBottom: '10px' }}
              >
                <ArrowBackIcon />
              </IconButton>
              <Typography
                variant="h5"
                gutterBottom
                align="left"
                style={{ color: "#64001D", fontWeight: "bold", marginBottom: "20px" }}
              >
                Crear un Instituto
              </Typography>
              </Box>
            <InstiForm />
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}

export default add_users;
