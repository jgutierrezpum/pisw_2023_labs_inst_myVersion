import React, { useState, useEffect } from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {
  Container,
  Paper,
  Typography,
  TextField,
  Table,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Button,
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { API_BASE_URL } from '../js/config';

function ListaLab() {
  const [Rol, setLab] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const navigate = useNavigate();
  const [openDialog, setOpenDialog] = useState(false);
  const [labToDelete, setLabToDelete] = useState(null);

  useEffect(() => {
    const fetchLab = async () => {
      try {
        const token = localStorage.getItem('token');

        if (!token) {
          console.error('Token de autenticación no encontrado en el localStorage.');
          return;
        }

        const response = await axios.get(`${API_BASE_URL}registroLaboratorioPublico`, {
          headers: {
            'Authorization': `Bearer ${token}`,
            'Accept': 'application/json',
          },
        });

        if (response.status === 200) {
          const rawData = response.data.publiclabs;
          const filteredData = rawData.filter(entry => entry.estado === true);
          const transformedData = filteredData.map(entry => ({
            laboratorio: entry.laboratorio.nombre,
            responsable: `${entry.coordinador.nombre} ${entry.coordinador.apellido_paterno} ${entry.coordinador.apellido_materno}`,
            estado: entry.estado,
            area: entry.area.nombre,
            coordinador_id: entry.coordinador.usuario_id,
            laboratorio_id: entry.laboratorio.laboratorio_id,
            area_id: entry.area.area_id,
            disciplinas: entry.disciplinas.map(disciplina => disciplina.nombre).join(', '),
            ubicacion: entry.ubicacion,
            registro_id: entry.registro_id,
            mision: entry.mision,
            vision: entry.vision,
            historia:  entry.historia,
          }));

          setLab(transformedData);
        } else {
          console.error('Error en la respuesta de la API:', response.status);
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchLab();
  }, []);

  const handleModificarInfo = (labToEdit) => {
    navigate('/update_resp_lab', { state: { labToEdit } });
  };

  const handleVerInfo = (userToEdit) => {
    navigate('/info_laboratorio', { state: { userToEdit } });
  };

  const handleDeleteInfo = (labId) => {
    setLabToDelete(labId);
    setOpenDialog(true);
  };

  const confirmDelete = async (labId) => {
    try {
      const token = localStorage.getItem('token');
      const response = await axios.delete(`${API_BASE_URL}registroLaboratorio/${labId}`, {
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });

      if (response.status === 200) {
        const updatedLabs = Rol.filter(lab => lab.registro_id !== labId);
        setLab(updatedLabs);
      } else {
        console.error('Error en la respuesta de la API:', response.status);
      }
    } catch (error) {
      console.error('Error:', error);
    } finally {
      setOpenDialog(false);
    }
  };

  const filteredLabs = Rol.filter((lab) =>
    lab.laboratorio.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const paginatedLabs = filteredLabs.slice(
    currentPage * rowsPerPage,
    currentPage * rowsPerPage + rowsPerPage
  );

  const totalPages = Math.ceil(filteredLabs.length / rowsPerPage);

  return (
    <Container
      maxWidth="xl"
      sx={{
        minHeight: '50vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '4%',
      }}
    >
      <Paper elevation={3} style={{ padding: '50px', width: '100%' }}>
        <Typography variant="h4" align="left" gutterBottom sx={{ color: '#64001D', fontWeight: 'bold' }}>
          Laboratorios
        </Typography>

        <Grid container spacing={2} alignItems="center">
          <Grid item xs={12} sm={9} md={10}>
            <Button
              component={Link}
              to="/add_gestion"
              variant="contained"
              style={{ backgroundColor: '#64001D', color: '#FFFFFF' }}
            >
              Asignar responsable
            </Button>
          </Grid>
          <Grid item xs={12} sm={3} md={2}>
            <TextField
              label="Buscar"
              variant="outlined"
              margin="normal"
              InputLabelProps={{ style: { color: '#64001D' } }}
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
            />
          </Grid>
        </Grid>

        <TableContainer style={{ maxHeight: '50vh' }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell sx={{ textAlign: 'center', fontWeight: 'bold', fontSize: '100%', Width: '30%' }}>Laboratorio</TableCell>
                <TableCell sx={{ textAlign: 'center', fontWeight: 'bold', fontSize: '100%', Width: '30%' }}>Responsable</TableCell>
                <TableCell sx={{ textAlign: 'center', fontWeight: 'bold', fontSize: '100%', Width: '40%' }}>Actividad</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {paginatedLabs.map((lab, index) => (
                <TableRow key={index}>
                  <TableCell sx={{ textAlign: 'center' }}>{lab.laboratorio}</TableCell>
                  <TableCell sx={{ textAlign: 'center' }}>{lab.responsable}</TableCell>
                  <TableCell sx={{ textAlign: 'center' }}>
                    <Grid container alignItems="center" justifyContent="center">
                      <Grid item xs={12} sm={4}>
                        <IconButton
                          style={{ backgroundColor: '#64001D', color: '#FFFFFF' }}
                          onClick={() => handleVerInfo(lab)}
                        >
                          <VisibilityIcon />
                        </IconButton>
                      </Grid>
                      <Grid item xs={12} sm={4}>
                        <Button
                          variant="contained"
                          style={{ backgroundColor: '#64001D', color: '#FFFFFF' }}
                          onClick={() => handleModificarInfo(lab)}
                        >
                          Modificar
                        </Button>
                      </Grid>
                      <Grid item xs={12} sm={4}>
                        <Button
                          variant="contained"
                          style={{ backgroundColor: '#64001D', color: '#FFFFFF' }}
                          onClick={() => handleDeleteInfo(lab.registro_id)}
                        >
                          Eliminar
                        </Button>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <div style={{ display: 'flex', justifyContent: 'center', margin: '20px' }}>
          {[...Array(totalPages)].map((_, index) => (
            <Button
              key={index}
              variant={currentPage === index ? 'contained' : 'outlined'}
              onClick={() => setCurrentPage(index)}
              style={{ margin: '0 5px' }}
            >
              {index + 1}
            </Button>
          ))}
        </div>

        {/* Ventana de confirmación */}
        <Dialog
          open={openDialog}
          onClose={() => setOpenDialog(false)}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Confirmación de Eliminación"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              ¿Estás seguro de que quieres eliminar responsable de laboratorio?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpenDialog(false)} color="primary">
              Cancelar
            </Button>
            <Button onClick={() => confirmDelete(labToDelete)} color="primary" autoFocus>
              Eliminar
            </Button>
          </DialogActions>
        </Dialog>
      </Paper>
    </Container>
  );
}

export default ListaLab;
