import React, { useRef, useState } from 'react';
import axios from 'axios';
import { Grid, TextField, Button, InputAdornment, IconButton } from '@mui/material';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import { useLocation, useNavigate } from 'react-router-dom';
import { API_BASE_URL } from '../js/config';

// Definir estilos para el componente
const styles = {
  label: {
    color: '#555',
  },
 
  button: {
    backgroundColor: '#64001D',
    '&:hover': {
      backgroundColor: '#44001F',
    },
    color: '#fff',
    padding: '10px 30px',
  },
  fileInput: {
    display: 'none',
  },
  inputWithIcon: {
    paddingRight: 0
  },
};


// Componente funcional EquipmentForm
function PoiForm() {
  // Obtener la ubicación y datos del laboratorio de la ubicación actual  
  const location = useLocation();
  const labData = location.state?.labData;

  // Configurar la navegación  
  const navigate = useNavigate();
  // Referencia para el input de archivo
  const fileInputRef = useRef();

  // Estado para el formulario  
  const [formData, setFormData] = useState({
    fileName: labData?.nombre_poi || "",
    selectedFile: null,
    año: labData?.año || ""
  });

  // Manejar cambios en los campos del formulario
  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };


  // Manejar cambios en el input de archivo
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setFormData((prevData) => ({
      ...prevData,
      selectedFile: file,
      fileName: file.name
    }));
  };

  // Mostrar el selector de archivo al hacer clic en el icono
  const triggerFileSelect = () => {
    fileInputRef.current.click();
  };
  // Función para enviar el formulario y subir el archivo  
  const subirArchivo = async () => {
    try {
      // Desestructurar datos del formulario      
      const { selectedFile, fileName, año } = formData;
      const file = selectedFile;
      // Crear un objeto FormData y agregar datos
      const formPayload = new FormData(); // Aquí cambiamos el nombre
      //formPayload.append("instituto_id", labData.instituto_id);
      formPayload.append("nombre_poi", fileName);
      formPayload.append("año", año);
      formPayload.append("archivo_poi", file);

      // Verificar existencia de datos y token      
      if (!labData) {
        console.error('instituto_id no está definido.');
        return;
      }

      const token = localStorage.getItem('token');
      if (!token) {
        console.error('Token de autenticación no encontrado en el localStorage.');
        return;
      }

      // Configuración de encabezados para la solicitud
      const config = {
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
          "Content-Type": "multipart/form-data",
        },
      };

      // Inicializar la variable de respuesta
      let response;

      // Verificar si el equipo ya tiene un ID (solicitud PUT) o no (solicitud POST)      
      if (labData.poi_id) {
        const apiUrl = `${API_BASE_URL}directores/pois/${labData.poi_id}`;
        // Si labData.nombre_documento tiene datos, realiza una solicitud PUT
        response = await axios.post(apiUrl, formPayload, config);
      } else {
        const apiUrl = `${API_BASE_URL}directores/pois/`;
        // Si labData.nombre_documento no tiene datos, realiza una solicitud POST
        response = await axios.post(apiUrl, formPayload, config);
      }

      // Registrar la respuesta en la consola      

      // Redirige al usuario después de una respuesta exitosa
      if (response.status === 200) {
        // Aquí puedes agregar más condiciones si es necesario, como verificar campos específicos en response.data

        // Redirige al usuario después de una respuesta exitosa
        navigate('/res_poi_insti', { state: { labData } });
      } else {
        console.error("La solicitud no fue exitosa:", response.status, response.data);
      }

      console.log(formData);

    } catch (error) {
      if (error.response) {
        // La solicitud fue realizada, pero el servidor respondió con un código de estado fuera del rango 2xx
        console.error("Error de respuesta del servidor:", error.response.status, error.response.data);
      } else if (error.request) {
        // La solicitud fue realizada, pero no se recibió ninguna respuesta del servidor
        console.error("No se recibió respuesta del servidor.");
      } else {
        // Se produjo un error durante la configuración de la solicitud
        console.error("Error de configuración de la solicitud:", error.message);
      }
    }
  };


  // Renderizar el formulario
  return (
    <form style={{ backgroundColor: '#FFFFFF', padding: '20px', borderRadius: '8px', boxShadow: '0px 0px 5px rgba(0, 0, 0, 0.1)' }}>
      <Grid container spacing={3}>
        {/* Input de imagen */}
        <Grid item xs={12} md={6}>
          <TextField
            fullWidth
            name="imagenReferencial"
            label="Archivo"
            variant="outlined"
            InputLabelProps={{ style: styles.label }}
            style={styles.textField}
            value={formData.fileName || " "}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end" style={styles.inputWithIcon}>
                  <IconButton edge="end" onClick={triggerFileSelect}>
                    <AttachFileIcon color="primary" />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
          {/* Input oculto de tipo archivo */}
          <input type="file" ref={fileInputRef} style={styles.fileInput} onChange={handleFileChange} />
        </Grid>
        {/* Otros campos del formulario */}
        <Grid item xs={12} md={6}>
          <TextField
            fullWidth
            name="año"
            label="Año"
            variant="outlined"
            InputLabelProps={{ style: styles.label }}
            style={styles.textField}
            value={formData.año}
            onChange={handleChange}
          />
        </Grid>
        <Grid item xs={12}>
          {/* Botón para guardar el formulario */}
          <Button variant="contained" style={styles.button} onClick={subirArchivo}>Guardar</Button>
        </Grid>
      </Grid>
    </form>
  );

}

export default PoiForm;
