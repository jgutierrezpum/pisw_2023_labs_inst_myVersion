<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Publicacion;
use Illuminate\Database\QueryException;

class PublicacionController extends Controller
{
    // Funcion para listar las publicaciones
    public function index($idregistrarlaboratorio){
        
        // Verificamos que el id del laboratorio sea numerico
        $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
        
        if($idregistrarlaboratorio === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        
        // Obtenemos las publicaciones
        $publicaciones = Publicacion::where('registro_id','=',$idregistrarlaboratorio)
                                    ->orderBy('registro_id', 'asc')
                                    ->with('laboratorio')
                                    ->get();

        // Verificar si existen publicaciones
        if(count($publicaciones) == 0) {
            return response()->json(['message' => 'No cuenta con publicaciones' ], 404);
        }

        // Retornar las publicaciones
        return response()->json(['publicaciones'=>$publicaciones], 200);
    }

    // Funcion para registrar una nueva publicacion
    public function store(Request $request) {

        // Validamos lso valores necesarios
        $request->validate([
            'titulo' => 'required|string',
            'link' => 'required|string',
            'registro_id' => 'required|integer'
        ]);

        try {
            // Seteamos un objeto nuevo con valores de la peticion
            $publicacion = new Publicacion;
            $publicacion->titulo = $request->titulo;
            $publicacion->link = $request->link;
            $publicacion->registro_id = $request->registro_id;
            $publicacion->estado = true;

            // Guardamos el valor en la db
            $publicacion->save();

            // Retornamos operacion exitosa
            return response()->json(['status'=> 'Creado correctamente'], 200);

        } catch (QueryException $e){
            // Devolvemos el error en caso haya uno
            return response()->json(['message'=>'Ocurrio un error', 'error'=> $e]);
        }
    }

    // Funcion para mostrar informacion detallada de una publicacion
    public function show($idpublicacion){ 

        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        //Obtenemos la publicacion
        $publicacion = Publicacion::where('publicacion_id', $idpublicacion)->first();

        // Verificamos que la publicacion buscada exista
        if($publicacion){
            return response()->json(['publicacion'=> $publicacion], 200);
        }

        // Retornamos que no hay la publicacion
        return response()->json(['message' => 'Publicacion no encontrada'], 404);
    }

    // Funcion para modificar una publicacion
    public function update(Request $request, $idpublicacion){

        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        //Obtenemos la publicacion
        $publicacion = Publicacion::where('publicacion_id', $idpublicacion)->first();

        try {
            // Seteamos un objeto nuevo con valores de la peticion
            $publicacion->titulo = $request->titulo;
            $publicacion->link = $request->link;
            $publicacion->registro_id = $request->registro_id;

            // Guardamos el valor en la db
            $publicacion->update();

            // Retornamos operacion exitosa
            return response()->json(['status'=> 'Actualizado correctamente'], 200);

        } catch (QueryException $e){
            // Devolvemos el error en caso haya uno
            return response()->json(['message'=>'Ocurrio un error', 'error'=> $e]);
        }

    }

    // Funcion para eliminar una publicacion
    public function delete($idpublicacion){

        // Verificamos que el id del laboratorio sea numerico
        $idpublicacion = (int) $idpublicacion;
        
        if($idpublicacion === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        // Obtenemos la publicacion
        $publicacion = Publicacion::where('publicacion_id','=',$idpublicacion)->first();

        // Verificamos que exista la publicacion y tenga el estado true
        if($publicacion && $publicacion->estado){

            $publicacion->estado = false; //Cambiamos el estado a false

            // Guardamos los cambios en la db
            $publicacion->save();

            // Retornamos operacion exitosa
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Publicacion no encontrada'], 404);
        }
    }
}
