<?php

namespace App\Http\Controllers;

use App\Models\Documento;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentoController extends Controller
{
  public function index()
  {
      $documentos = Documento::all();
      return response()->json(['documentos' => $documentos]);
  }

  public function show($iddocumento)
  {
    $idDocumento = (int) $iddocumento;
    //Verificar que el idlaboratorio es de tipo integer
    if($idDocumento === 0) {
        return response()->json(['message' => 'Tipo de dato no válido']);
    }
     
    $documento = Documento::find($idDocumento);
    return response()->json(['documento' => $documento]);
  }

  public function store(Request $request)
  {
      $request->validate([
        'registro_id' => 'required|integer',
        'nombre_documento' => 'required|string|max:255',
        'archivo_documento' => 'required|file',
      ]);

      $documento = new Documento;
      $documento->nombre_documento = $request->nombre_documento;
      $documento->registro_id = $request->registro_id;

      if($request->hasFile('archivo_documento')) {
        $path = $request->file('archivo_documento')->store('documentos');
        $documento->archivo_documento = $path;
      }

      $documento->save();

      return response()->json(['documento' => $documento, 'message' => 'Documento creado exitosamente']);
  }

  public function update(Request $request, $iddocumento)
  {
      $request->validate([
        'nombre_documento' => 'required|string|max:255',
        'archivo_documento' => 'required|file',
      ]);
      
      try {
        $idDocumento = (int) $iddocumento;
        //Verificar que el idlaboratorio es de tipo integer
        if($idDocumento === 0) {
          return response()->json(['message' => 'Tipo de dato no válido']);
        }
        
        
        $documento = Documento::find($idDocumento); 
        
        if(!$documento) {
          return response()->json(['message' => 'Documento no encontrado']);
        }
  
        $documento->nombre_documento = $request->nombre_documento;

        if($request->hasFile('archivo_documento')) {
            Storage::delete($documento->archivo_documento); // Eliminamos el archivo antiguo
            $path = $request->file('archivo_documento')->store('documentos'); // Guardamos el nuevo archivom
            $documento->archivo_documento = $path; // Actualizamos la ruta del archivo
        }
        
        
        $documento->update();
  
        return response()->json(['documento' => $documento, 'message' => 'Documento actualizado exitosamente']);
      } catch (QueryException $e) {
        return response()->json(['status' => 'error', 'message' => $e]);
      }

  }

  public function delete($iddocumento)
  {
    $idDocumento = (int) $iddocumento;
    //Verificar que el idlaboratorio es de tipo integer
    if($idDocumento === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $documento = Documento::find($iddocumento);
    
    if(!$documento) {
      return response()->json(['message' => 'Documento no encontrado']);
    }
    // Storage::delete($documento->ruta_documento); // Eliminamos el archivo
    $documento->estado = false; // Eliminamos el registro
    $documento->save();

    return response()->json(['message' => 'Documento eliminado exitosamente']);
  }

  public function download($id)
  {

      $documento = Documento::find($id);
      // $documentName = "{$documento->nombre_documento}.pdf";
      return response()->download(storage_path("app/{$documento->archivo_documento}"), $documento->nombre_documento);
      // return response()->download(storage_path("app/{$documento->archivo_documento}"))->header('X-Filename', $documentName);
  }

}
