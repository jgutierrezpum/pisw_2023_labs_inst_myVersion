<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\OperadorLaboratorio;
use Illuminate\Http\Request;

class AsignarOperadorController extends Controller
{
  // Listar los operadores de un laboratorio
  public function listarOperadoresLaboratorio($idregistrarlaboratorio)
  {

    $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
    //Verificar que el idlaboratorio es de tipo integer
    if ($idregistrarlaboratorio === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $operadores = OperadorLaboratorio::where('registro_id', $idregistrarlaboratorio)->orderBy('asignar_id', 'asc')
      ->with('operador')
      ->get();

    if (count($operadores) == 0) {
      return response()->json(['message' => 'El laboratorio no tiene operadores asignados'], 404);
    }

    return response()->json(['operadores' => $operadores], 200);
  }

  public function asignarOperadorLaboratorio(Request $request, $idregistrarlaboratorio)
  {
    $idregistrarlaboratorio = (int) $idregistrarlaboratorio;
    //Verificar que el idlaboratorio es de tipo integer
    if ($idregistrarlaboratorio === 0) {
      return response()->json(['message' => 'Tipo de dato no válido']);
    }

    $request->validate([
      'operador_id' => 'required',
    ]);

    $asignarOperador = new OperadorLaboratorio;
    $asignarOperador->operador_id = $request->operador_id;
    $asignarOperador->registro_id = $idregistrarlaboratorio;
    $asignarOperador->save();

    return response()->json(['message' => 'Operadores asignados correctamente']);
  }
}
