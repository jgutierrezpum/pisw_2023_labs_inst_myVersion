<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     * Mostrar areas
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $areas = Area::orderBy('area_id', 'asc')->get();
        return response()->json(['areas' => $areas]);
    }

    /**
     * Registrar area
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if(!empty(trim($request->get('nombre')))){
                $area = new Area;
                $area->nombre = $request->get('nombre');
                $area->save();
                return response()->json(['status' => 'Creado Correctamente']); 
            }else{
                return response()->json(['message' => 'El campo nombre es requerido']);
            }
            return response()->json(['status' => 'Creado Correctamente']); 
        }catch (QueryException $e){
            return response()->json(['message'=> 'El área ya esta registrada']);
        }
    }

    /**
     * Mostrar area por id
     *
     * @param  int $idarea
     * @return \Illuminate\Http\Response
     */
    public function show($idarea)
    {
        $idarea = (int) $idarea;
        //Verificar que el idarea es de tipo integer
        if($idarea === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $area = Area::where('area_id','=',$idarea)->first();
        if($area && $area->estado){
            return response()->json(['area' => $area]);
        } else {
            return response()->json(['message' => 'Área no encontrada'], 404);
        }
    }

    /**
     * Actualizar area  por id
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $idarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idarea)
    {
        $idarea = (int) $idarea;
        //Verificar que el idarea es de tipo integer
        if($idarea === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }
        $area = Area::where('area_id','=',$idarea)->first();
        if($area){
            try{
                if(!empty(trim($request->get('nombre')))){
                    $area->nombre = $request->get('nombre');
                    $area->update();
                    return response()->json(['status' => 'Actualizado Correctamente']);
                }else{
                    return response()->json(['message' => 'El campo nombre es requerido']);
                }
            } catch (QueryException $e){
                return response()->json(['message'=> 'El área ya esta registrada']);
            }
        } else {
            return response()->json(['message' => 'Área no encontrada'], 404);
        }
    }

    /**
     * Eliminar area por id
     *
     * @param  int $idarea
     * @return \Illuminate\Http\Response
     */
    public function delete($idarea)
    {
        $idarea = (int) $idarea;
        //Verificar que el idarea es de tipo integer
        if($idarea === 0) {
            return response()->json(['message' => 'Tipo de dato no válido']);
        }

        $area = Area::where('area_id','=',$idarea)->first();
        if($area){
        // if($area && $area->estado){
            $area->estado = false;
            $area->save();
            return response()->json(['status' => 'Eliminado Correctamente']); 
        } else {
            return response()->json(['message' => 'Área no encontrada'], 404);
        }
    }
}
