<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('verificarRol:1', ['only' => ['store', 'update', 'delete']]);
    }

    public function index()
    {
        //Mostrar los dat
        $usuarios = Usuario::orderBy('idusuario', 'asc')->get();
        return response()->json(['usuarios' => $usuarios]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Agregamos un usuario


        $usuario = new Usuario;
        $usuario->documento = $request->get('documento');
        $usuario->nombreusuario = $request->get('nombreusuario');
        $usuario->apellidopa = $request->get('apellidopa');
        $usuario->apellidoma = $request->get('apellidoma');
        $usuario->direccionusuario = $request->get('direccionusuario');
        $usuario->telefonousuario = $request->get('telefonousuario');
        $usuario->correo = $request->get('correo');
        $usuario->passwd = $request->get('passwd');
        $usuario->idrol = $request->get('idrol');
        $usuario->estado = $request->get('estado');
        $usuario->save();
        return response()->json(['status' => 'Creado Correctamente']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idusuario)
    {

        $usuario = Usuario::all()->where('idusuario', '=', $idusuario);
        return response()->json(['usuario' => $usuario]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $idusuario)
    {
        //Actualizamos el registro
        $usuario = Usuario::all()->where('idusuario','=',$idusuario);
        $usuario->documento=$request->documento;
        $usuario->nombreusuario=$request->nombreusuario;
        $usuario->apellidopa=$request->apellidopa;
        $usuario->apellidoma=$request->apellidoma;
        $usuario->direccionusuario=$request->direccionusuario;
        $usuario->telefonousuario=$request->telefonousuario;
        $usuario->correo=$request->correo;
        $usuario->passwd=$request->passwd;
        $usuario->idrol=$request->idrol;
        $usuario->estado=$request->estado;
        $usuario->update();
        return response()->json(['status' => 'Actualizado Correctamente']);
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($idusuario)
    {
        //
        $usuario =  Usuario::all()->where('idusuario', '=', $idusuario)->first();

        if (is_null($usuario)) {
            return response()->json('No se pudo realizar correctamente la operación', 404);
        }

        $usuario->delete();
        return response()->json(['status' => 'Eliminado Correctamente']);
    }
}
