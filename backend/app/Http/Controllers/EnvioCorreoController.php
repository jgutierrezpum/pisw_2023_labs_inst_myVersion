<?php

namespace App\Http\Controllers;

use App\Mail\Mailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Mailer\Exception\TransportException;

class EnvioCorreoController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function correo(Request $request)
    {
        $to = $request->get('to');
        $message = $request->get('message');
        if(filter_var($to, FILTER_VALIDATE_EMAIL)){
            try{
                Mail::to($to)->send(new Mailer($message));
                return response()->json(['status' => 'Correo enviado exitosamente']);
            } catch(TransportException $e){
                return response()->json(['message'=> 'Error en el envío del correo']);
            }
        }else{
            return response()->json(['message'=> 'Correo inválido']);
        }
    }
}
