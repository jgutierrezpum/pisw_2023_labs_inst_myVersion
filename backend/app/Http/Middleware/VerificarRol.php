<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerificarRol
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
   * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
   */
  public function handle(Request $request, Closure $next, ...$roles)
  {
    $user = $request->user();
    // Verificar si el usuario tiene asignado un rol
    if ($user && $user->rol) {

      foreach ($roles as $rol) {
        if ($user->rol->rol_id == $rol) {
          return $next($request);
        }
      }
    }
    return response()->json(['error' => "Acceso no autorizado"], 403);
  }
}
