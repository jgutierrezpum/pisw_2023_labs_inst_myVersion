<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegistroLaboratorio extends Model
{
    use HasFactory;

    protected $table = 'registro_laboratorios'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'registro_id'; // Clave primaria personalizada

    protected $fillable = [
      'coordinador_id',
      'laboratorio_id',
      'area_id',
      //'disciplina_id',
      'ubicacion',
      'servicios',
      'mision',
      'vision',
      'historia',
      'estado'
    ];

    protected $casts = [
      'servicios' => 'array',
    ];
    
    protected $with = [
      'disciplinas'
    ];

    public function coordinador(){
      return $this->hasOne(User::class, 'usuario_id', 'coordinador_id');
    }

    public function laboratorio(){
      return $this->hasOne(Laboratorio::class, 'laboratorio_id', 'laboratorio_id');
    }

    public function area(){
      return $this->hasOne(Area::class, 'area_id', 'area_id');
    }

    /*public function disciplina(){
      return $this->hasOne(Disciplina::class, 'disciplina_id', 'disciplina_id');
    }*/
    public function disciplinas()
    {
        return $this->belongsToMany(Disciplina::class, 'registro_laboratorio_disciplina', 'registro_id', 'disciplina_id');
    }
}
