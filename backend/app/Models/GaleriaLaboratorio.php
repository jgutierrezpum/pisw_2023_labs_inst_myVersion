<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GaleriaLaboratorio extends Model
{
    use HasFactory;

    protected $table = 'galeria_laboratorios'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'galeria_id'; // Clave primaria personalizada

    protected $fillable = [
      'registro_id',
      'nombre_imagen',
      'imagen_galeria',
      'descripcion',
      'estado'
    ];
    
    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->imagen_galeria);
    }

}
