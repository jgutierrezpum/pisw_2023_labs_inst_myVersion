<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
    use HasFactory;
    // Nombre de la tabla en la base de datos
    protected $table = 'disciplinas';
    protected $primaryKey = 'disciplina_id';
    // Define las propiedades fillable para permitir la asignación en masa
    protected $fillable = [
        'nombre',
        'estado'
    ];
    protected $hidden = ['created_at', 'updated_at'];

    public function laboratorios()
    {
        return $this->belongsToMany(RegistroLaboratorio::class, 'registro_laboratorio_disciplina', 'disciplina_id', 'registro_id');
    }
}
