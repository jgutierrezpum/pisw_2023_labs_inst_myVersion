<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicacionInstituto extends Model
{
    use HasFactory;

    // Table
    protected $table = 'publicacion_institutos';

    // Llave primaria
    protected $primaryKey = 'publicacion_id';

    protected $fillable = [
        'instituto_id',
        'titulo',
        'link',
        'estado'
      ];

    public function instituto(){
        return $this->hasOne(Instituto::class, 'instituto_id', 'instituto_id');
    }
}
