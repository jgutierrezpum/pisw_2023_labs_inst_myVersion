<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poi extends Model
{
    use HasFactory;

    protected $table = 'poi'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'poi_id'; // Clave primaria personalizada

    protected $fillable = [
      'instituto_id',
      'año',
      'nombre_poi',
      'archivo_poi',
      'estado',
    ];
    
    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        // return asset($this->imagen_equipo);
        return asset('storage/' . $this->archivo_poi);
    }

    // Relación con el modelo Instituto
    public function instituto()
    {
        return $this->belongsTo(Instituto::class, 'instituto_id', 'instituto_id');
    } 
}
