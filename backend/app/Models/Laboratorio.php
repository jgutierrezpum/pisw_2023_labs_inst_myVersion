<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    use HasFactory;
    // Nombre de la tabla en la base de datos
    protected $table = 'laboratorios';
    protected $primaryKey = 'laboratorio_id';
    // Define las propiedades fillable para permitir la asignación en masa
    protected $fillable = [
        'nombre',
        'estado'
    ];
    protected $hidden = ['created_at', 'updated_at'];
}
