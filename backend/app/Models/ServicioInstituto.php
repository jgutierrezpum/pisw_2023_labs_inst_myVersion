<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicioInstituto extends Model
{
    use HasFactory;

    protected $table = 'servicios_institutos'; // Nombre real de tu tabla en la base de datos
    protected $primaryKey = 'servicio_id'; // Nombre de la clave primaria
    protected $fillable = ['instituto_id',
        'servicio', 'materiales', 'mano_obra',
        'ci_mano_obra', 'ci_deprec_equi',
        'ci_deprec_edif', 'ci_util_limp',
        'ci_util_aseo', 'ci_mantto', 'ci_servicios' , 'estado']; // Campos que se pueden llenar masivamente

    // Relación con el modelo Instituto
    public function instituto()
    {
        return $this->belongsTo(Instituto::class, 'instituto_id', 'instituto_id');
    }
}
