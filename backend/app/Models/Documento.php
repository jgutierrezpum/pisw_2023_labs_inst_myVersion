<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    use HasFactory;

    protected $table = 'documentos'; // Nombre de la tabla en la base de datos

    protected $primaryKey = 'documento_id'; // Clave primaria personalizada

    protected $fillable = [
        'registro_id',
        'nombre_documento',
        'archivo_documento',
        'estado',
    ];

}
