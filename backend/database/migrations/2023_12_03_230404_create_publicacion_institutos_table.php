<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicacion_institutos', function (Blueprint $table) {
            $table->id('publicacion_id');
            $table->unsignedInteger('instituto_id');
            $table->string('titulo');
            $table->text('link');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publicacion_institutos', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
        });
        Schema::dropIfExists('publicacion_institutos');
    }
};
