<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_laboratorios', function (Blueprint $table) {
            $table->id('registro_id');
            $table->unsignedInteger('coordinador_id');
            $table->unsignedInteger('laboratorio_id');
            $table->unsignedInteger('area_id');
            //$table->unsignedInteger('disciplina_id');
            $table->string('ubicacion', 255)->nullable();
            $table->jsonb('servicios')->nullable();
            $table->text('mision')->nullable();
            $table->text('vision')->nullable();
            $table->text('historia')->nullable();
            $table->boolean('estado')->default(true);
            $table->timestamps();

            //FOREIGN KEY en Users
            $table->foreign('coordinador_id')->references('usuario_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            //FOREIGN KEY en Laboratorios
            $table->foreign('laboratorio_id')->references('laboratorio_id')->on('laboratorios')->onUpdate('cascade')->onDelete('cascade');
            //FOREIGN KEY en Area
            $table->foreign('area_id')->references('area_id')->on('areas')->onUpdate('cascade')->onDelete('cascade');
            //FOREIGN KEY en Disciplinas
            //$table->foreign('disciplina_id')->references('disciplina_id')->on('disciplinas')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('registro_laboratorios', function (Blueprint $table) {
        $table->dropForeign(['coordinador_id']);
        $table->dropForeign(['laboratorio_id']);
        $table->dropForeign(['area_id']);
        //$table->dropForeign(['disciplina_id']);
      });
      Schema::dropIfExists('registro_laboratorios');
        // Schema::dropIfExists('registro_laboratorios');
    }
};
