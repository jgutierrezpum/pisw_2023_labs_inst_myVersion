<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registro_laboratorio_disciplina', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('registro_id');
            $table->unsignedBigInteger('disciplina_id');
            $table->timestamps();

            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onDelete('cascade');
            $table->foreign('disciplina_id')->references('disciplina_id')->on('disciplinas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registro_laboratorio_disciplina');
    }
};
