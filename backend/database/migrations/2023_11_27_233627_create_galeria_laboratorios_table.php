<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_laboratorios', function (Blueprint $table) {

            $table->id('galeria_id');
            $table->unsignedInteger('registro_id');
            $table->string('nombre_imagen')->nullable();
            $table->text('imagen_galeria')->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('estado')->default(true);

            $table->timestamps();
            
            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('galeria_laboratorios', function (Blueprint $table) {
            $table->dropForeign(['registro_id']);
        });
        Schema::dropIfExists('galeria_laboratorios');
    }
};
