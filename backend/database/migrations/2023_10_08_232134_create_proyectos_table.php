<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
            $table->id('proyecto_id');
            $table->unsignedInteger('registro_id');
            $table->string('nombre_proyecto', 255);
            $table->string('investigador_principal', 255);
            $table->jsonb('coinvestigadores');
            $table->string('nombre_imagen')->nullable();
            $table->string('imagen_referencial')->nullable();
            
            //Campo nuevo
            $table->string('etapa', 255)->nullable();
            $table->string('duracion', 255)->nullable();
            $table->jsonb('lineas_investigacion')->nullable();

            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_finalizacion')->nullable();

            $table->string('doi', 255);
            $table->text('resumen');
            $table->string('iba', 50);
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('registro_id')->references('registro_id')->on('registro_laboratorios')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('proyectos', function (Blueprint $table) {
        $table->dropForeign(['registro_id']);
      });

      Schema::dropIfExists('proyectos');
    }
};
