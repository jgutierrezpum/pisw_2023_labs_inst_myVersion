<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operador_institutos', function (Blueprint $table) {
            $table->id('asignar_id');
            $table->unsignedInteger('operador_id');
            $table->unsignedInteger('instituto_id');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            //FOREIGN KEY en Users
            $table->foreign('operador_id')->references('usuario_id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            //FOREIGN KEY en Registro
            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operador_institutos', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
            $table->dropForeign(['operador_id']);
        });

        Schema::dropIfExists('operador_institutos');
    }
};
