<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_institutos', function (Blueprint $table) {
            $table->id('servicio_id');
            $table->unsignedInteger('instituto_id');
            $table->string('servicio');
            $table->double('materiales',10,2);
            $table->double('mano_obra',10,2);
            $table->double('ci_mano_obra',10,2);
            $table->double('ci_deprec_equi',10,2);
            $table->double('ci_deprec_edif',10,2);
            $table->double('ci_util_limp',10,2);
            $table->double('ci_util_aseo',10,2);
            $table->double('ci_mantto',10,2);
            $table->double('ci_servicios',10,2);
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_institutos');
    }
};
