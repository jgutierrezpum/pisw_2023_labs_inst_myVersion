<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Se crea el migrate de institutos
        Schema::create('funciones_instituto', function (Blueprint $table) {

            $table->id('funcion_id');
            $table->unsignedInteger('instituto_id');
            $table->string('funcion');
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('instituto_id')->references('instituto_id')->on('institutos')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //se aplica la validaciion de foreing 
        Schema::table('funciones_instituto', function (Blueprint $table) {
            $table->dropForeign(['instituto_id']);
        });
        Schema::dropIfExists('funciones_instituto');
    }
};
