<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->id('usuario_id');
      $table->string('nombre', 45);
      $table->string('apellido_paterno', 45)->nullable();
      $table->string('apellido_materno', 45)->nullable();
      $table->string('dni', 8)->nullable()->unique();
      $table->string('telefono', 9)->nullable();
      $table->string('correo')->unique();
      $table->string('contrasena');
      $table->string('direccion', 45)->nullable();
      $table->unsignedBigInteger('rol_id')->default(3);

      $table->text('resumen')->nullable();
      $table->date('fecha_eleccion')->nullable();
      $table->date('fecha_culminacion')->nullable();

      $table->string('categoria')->nullable();
      $table->string('regimen')->nullable();

      $table->boolean('estado')->default(true);
      $table->timestamps();
      $table->timestamp('email_verified_at')->nullable();
      $table->rememberToken();

      $table->foreign('rol_id')->references('rol_id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
    });

    //PRIMERA VERSION DE LA TABLA
    // Schema::create('users', function (Blueprint $table) {
    //     $table->id();
    //     $table->string('email')->unique();
    //     $table->timestamp('email_verified_at')->nullable();
    //     $table->string('password');
    //     $table->rememberToken();
    //     $table->string('documento', 45)->unique();
    //     $table->string('nombreusuario', 45);
    //     $table->string('apellidopa', 45)->nullable();
    //     $table->string('apellidoma', 45)->nullable();
    //     $table->string('direccionusuario', 45);
    //     $table->string('telefonousuario', 20);
    //     $table->unsignedBigInteger('idrol');
    //     $table->boolean('estado')->default(true);
    //     $table->timestamps();
    //     $table->foreign('idrol')->references('idrol')->on('roles')->onUpdate('cascade')->onDelete('cascade');
    // });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropForeign(['rol_id']);
    });

    Schema::dropIfExists('users');
  }
};
