<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\RegistroLaboratorio>
 */
class RegistroLaboratorioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'coordinador_id' => $this->faker->numberBetween(1, 100), // Reemplaza con valores apropiados
            'laboratorio_id' => $this->faker->numberBetween(1, 100),
            'area_id' => $this->faker->numberBetween(1, 100),
            'disciplina_id' => $this->faker->numberBetween(1, 100),
            'ubicacion' => $this->faker->text(50),
            'mision' => $this->faker->text(200),
            'vision' => $this->faker->text(200),
            'historia' => $this->faker->text(200),
            'estado' => true, // Probabilidad de que 'estado' sea verdadero (true)
        ];
    }
}
