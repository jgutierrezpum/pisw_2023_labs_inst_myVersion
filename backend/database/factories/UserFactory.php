<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->firstName,
            'apellido_paterno' => $this->faker->lastName,
            'apellido_materno' => $this->faker->lastName,
            'dni' => $this->faker->unique()->numerify('########'), // Genera un DNI de 8 dígitos único
            'telefono' => $this->faker->optional()->numerify('#########'), // Opcional, genera un número de 9 dígitos
            'correo' => $this->faker->unique()->safeEmail,
            'contrasena' => Hash::make('password'), // Asegúrate de establecer una contraseña segura
            'direccion' => $this->faker->optional()->text(45), // Opcional, genera una dirección
            'rol_id' => 1, // Reemplaza con los valores de rol apropiados
            'estado' => $this->faker->boolean(90), // Probabilidad de que 'estado' sea verdadero (true)
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),

        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
