<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DisciplinasTableSeeder extends Seeder
{
    /**
     * Seeder para llenar la tabla Disciplinas en la base de datos con los campos correspondientes a cada disciplina
     *
     * @return void
     */
    public function run()
    {
        DB::table('disciplinas')->insert([
            [
                'disciplina_id' => 1,
                'nombre' => 'Arte, educación y cultura',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 2,
                'nombre' => 'Biodiversidad, ecología y convervación',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 3,
                'nombre' => 'Biofertilizantes',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 4,
                'nombre' => 'Biología molecular y celular',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 5,
                'nombre' => 'Bioquímica y genética',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 6,
                'nombre' => 'Biorremediación',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 7,
                'nombre' => 'Calidad ambiental y salud humana',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 8,
                'nombre' => 'Computación gráfica e imágenes',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 9,
                'nombre' => 'Comunicación política y medios digitales',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 10,
                'nombre' => 'Corrupción y reforma del estado',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 11,
                'nombre' => 'Democracia, ciudadanía, derechos humanos, desarrollo y justicia',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 12,
                'nombre' => 'Desarrollo y crecimiento económico',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 13,
                'nombre' => 'Diversificación de lal oferta turística: Potencial turístico, producto y experiencias turísticas',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 14,
                'nombre' => 'Ecología de poblaciones',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 15,
                'nombre' => 'Ecología y conservación',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 16,
                'nombre' => 'Economía regional, sectorial e institucional',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 17,
                'nombre' => 'Enfermedades crónicas y degenerativas',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 18,
                'nombre' => 'Escenarios climáticos futuros y sus potenciales impactos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 19,
                'nombre' => 'Estadísticas y probabilidades',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 20,
                'nombre' => 'Estado, actores, conflictos sociales, ciudadanía y democracia',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 21,
                'nombre' => 'Ética profesional, trabajo social y derechos humanos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 22,
                'nombre' => 'Física de la materia condensada',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 23,
                'nombre' => 'Gerencia, administración funcional y gobierno',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 24,
                'nombre' => 'Gestión, políticas y didáctica educativas',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 25,
                'nombre' => 'Gestión pública, de empresa, de proyectos y recursos humanos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 26,
                'nombre' => 'Historia regional y comtemporánea',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 27,
                'nombre' => 'Humanidades, investigación y sociedad',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 28,
                'nombre' => 'Manejo sostenible de recursos biológicos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 29,
                'nombre' => 'Matemáticas',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 30,
                'nombre' => 'Materiales compuestos heterogéneos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 31,
                'nombre' => 'Metabolismo, fisiología y fisiopatología',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 32,
                'nombre' => 'Microbiología, parasitología e inmunología',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 33,
                'nombre' => 'Microestructura y propiedades de materiales metálicos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 34,
                'nombre' => 'Nano materiales avanzados para diversas aplicaciones',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 35,
                'nombre' => 'Neurociencias',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 36,
                'nombre' => 'Nutrición humana y seguridad alimentaria',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 37,
                'nombre' => 'Políticas públicas, de protección social y de grupos en situación de vulerabilidad',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 38,
                'nombre' => 'Productos naturales',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 39,
                'nombre' => 'Química ambiental',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 40,
                'nombre' => 'Reciclaje y valorización de residuos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 41,
                'nombre' => 'Recursos hídricos, energéticos, geológicos y edaficos',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 42,
                'nombre' => 'Redes TIC',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 43,
                'nombre' => 'Remediación y recuperación de ambientes degradados',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 44,
                'nombre' => 'Salud mental',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 45,
                'nombre' => 'Salud pública y entornos saludables',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 46,
                'nombre' => 'Sistemas inteligentes',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 47,
                'nombre' => 'Taxonomía',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'disciplina_id' => 48,
                'nombre' => 'Zoología',
                'estado' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
