<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RegistroSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //
    DB::table('registro_laboratorios')->insert([
      'coordinador_id' => 2,
      'laboratorio_id' => 1,
      'area_id' => 1,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('registro_laboratorios')->insert([
      'coordinador_id' => 2,
      'laboratorio_id' => 2,
      'area_id' => 1,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('registro_laboratorios')->insert([
      'coordinador_id' => 2,
      'laboratorio_id' => 3,
      'area_id' => 2,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
  }
}
