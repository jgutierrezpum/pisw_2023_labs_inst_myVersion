<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(DisciplinasTableSeeder::class);
        $this->call(AreasTableSeeder::class);
        $this->call(LaboratorioSeeder::class);
        $this->call(InstitutoSeeder::class);
        $this->call(RegistroSeeder::class);
        $this->call(EquipoSeeder::class);
        $this->call(ProyectoSeeder::class);
    }
}
