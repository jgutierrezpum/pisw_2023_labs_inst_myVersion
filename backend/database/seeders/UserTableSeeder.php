<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
  /**
   * Seeder para ingresar datos en la tabla Usuarios en la base de datos
   *
   * @return void
   */
  public function run()
  {
    DB::table('users')->insert([
      'correo' => 'admin@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '77777777',
      'nombre' => 'Admin',
      'apellido_paterno' => 'ApellidoPa',
      'apellido_materno' => 'ApellidoMa',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 1,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('users')->insert([
      'correo' => 'coordinador@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '12345678',
      'nombre' => 'COR',
      'apellido_paterno' => 'COR',
      'apellido_materno' => 'COR',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 2,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('users')->insert([
      'correo' => 'operador@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '87654321',
      'nombre' => 'OP',
      'apellido_paterno' => 'OP',
      'apellido_materno' => 'OP',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 3,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('users')->insert([
      'correo' => 'director@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '87654322',
      'nombre' => 'DIRECTOR',
      'apellido_paterno' => 'OP',
      'apellido_materno' => 'OP',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 4,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('users')->insert([
      'correo' => 'adminlab@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '87654323',
      'nombre' => 'ADMIN LABORATORIO',
      'apellido_paterno' => 'AP',
      'apellido_materno' => 'AM',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 6,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);
    DB::table('users')->insert([
      'correo' => 'admininst@unsa.edu.pe',
      'contrasena' => Hash::make('password'), // password
      'dni' => '87654324',
      'nombre' => 'ADMIN INSTITUTO',
      'apellido_paterno' => 'AP',
      'apellido_materno' => 'AM',
      'direccion' => 'Av Parra',
      'telefono' => '987654321',
      'rol_id' => 7,
      'estado' => true,
      'created_at' => Carbon::now(),
      'updated_at' => Carbon::now(),
    ]);


    DB::table('users')->insert([
      'correo' => 'driverad@unsa.edu.pe',
      'nombre' => 'diego',
      'contrasena' => Hash::make('password'), // password
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'lroquem@unsa.edu.pe',
      'nombre' => 'luis',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'jlucasb@unsa.edu.pe',
      'nombre' => 'jair',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'jgutierrezpum@unsa.edu.pe',
      'nombre' => 'joel',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'ihancco@unsa.edu.pe',
      'nombre' => 'indira',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'lcuevas@unsa.edu.pe',
      'nombre' => 'lizeth',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'hbustinzat@unsa.edu.pe',
      'nombre' => 'henry',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'jlopezr@unsa.edu.pe',
      'nombre' => 'jheyson',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);

    DB::table('users')->insert([
      'correo' => 'jmamanimora@unsa.edu.pe',
      'nombre' => 'juan',
      'contrasena' => Hash::make('password'),
      'rol_id' => 1,
    ]);
  }
  /* {
      
      DB::table('users')->insert([
        'correo' => 'driverad@unsa.edu.pe',
        'contrasena' => Hash::make('diegorivera'), // password
        'dni' => '72554540',
        'nombre' => 'Admin',
        'apellido_paterno' => 'Rivera',
        'apellido_materno' => 'Demanuel',
        'direccion' => 'as ss',
        'telefono' => '989989898',
        'rol_id' => 1,
        'estado' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
      ]);


    // }
    */
}
