<?php

namespace Tests\Unit;

use App\Http\Controllers\ProyectoController;
use App\Models\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\TestCase;

class ProyectoControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIndex()
    {
        // Crear un registro de laboratorio ficticio
        $registroId = 1;
        // Crear proyectos de ejemplo relacionados con el registro de laboratorio
        Proyecto::factory()->create([
            'registro_id' => $registroId,
            'nombre_proyecto' => 'Proyecto 1',
            'investigador_principal' => '1',
            'coinvestigadores' => '1',
            'doi' => 'DOI123',
            'resumen' => 'Resumen del Proyecto 1',
            'iba' => 'IBA123',
            'imagen_desarrollo_proyecto_url' => 'images/proyecto1.jpg',
        ]);
        Proyecto::factory()->create([
            'registro_id' => $registroId,
            'nombre_proyecto' => 'Proyecto 2',
            'investigador_principal' => '1',
            'coinvestigadores' => '1',
            'doi' => 'DOI456',
            'resumen' => 'Resumen del Proyecto 2',
            'iba' => 'IBA456',
            'imagen_desarrollo_proyecto_url' => 'images/proyecto2.jpg',
        ]);

        $proyectoController = new ProyectoController();
        $response = $proyectoController->index($registroId);

        $response->assertStatus(200)
            ->assertJsonStructure(['proyectos'])
            ->assertJsonCount(2, 'proyectos');
    }

    public function testStore()
    {
        Storage::fake('public'); // Configura un sistema de archivos falso para almacenamiento

        $file = UploadedFile::fake()->image('test-image.jpg');

        $data = [
            'registro_id' => 1,
            'nombre_proyecto' => 'Nuevo Proyecto',
            'investigador_principal' => 'John Doe',
            'coinvestigadores' => 'Jane Smith, Mark Johnson',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_desarrollo_proyecto_url' => $file,
        ];

        $proyectoController = new ProyectoController();
        $response = $proyectoController->store(Request::create('/api/proyectos', 'POST', $data));

        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);

        // Verifica que la imagen se haya almacenado correctamente
        self::assertFileExists(storage_path('app/public/images/' . $file->hashName()));
        //Storage::disk('public')->assertExists('images/' . $file->hashName());
    }

    public function testShow()
    {
        $proyecto = Proyecto::factory()->create([
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_desarrollo_proyecto_url' => 'images/proyecto.jpg',
        ]);

        $proyectoController = new ProyectoController();
        $response = $proyectoController->show($proyecto->proyecto_id);

        $response->assertStatus(200)
            ->assertJsonStructure(['proyecto']);
    }

    public function testUpdate()
    {
        $proyecto = Proyecto::factory()->create([
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_desarrollo_proyecto_url' => 'images/proyecto.jpg',
        ]);

        $data = [
            'nombre_proyecto' => 'Proyecto Actualizado',
            'investigador_principal' => 'Investigador Principal Actualizado',
            'coinvestigadores' => 'Coinvestigador 3, Coinvestigador 4',
            'doi' => 'DOI123',
            'resumen' => 'Resumen Actualizado',
            'iba' => 'IBA123',
            'imagen_desarrollo_proyecto_url' => UploadedFile::fake()->image('updated-image.jpg'),
        ];

        $proyectoController = new ProyectoController();
        $response = $proyectoController->update(Request::create("/api/proyectos/{$proyecto->proyecto_id}", 'PUT', $data), $proyecto->proyecto_id);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
        $proyecto = Proyecto::factory()->create([
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_desarrollo_proyecto_url' => 'images/proyecto.jpg',
        ]);

        $proyectoController = new ProyectoController();
        $response = $proyectoController->delete($proyecto->proyecto_id);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);

        // Verifica que el estado se haya actualizado a falso
        $this->assertFalse(Proyecto::find($proyecto->proyecto_id)->estado);
    }
}
