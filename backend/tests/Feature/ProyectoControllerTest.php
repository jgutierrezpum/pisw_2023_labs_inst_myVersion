<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;
use App\Models\Proyecto;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProyectoControllerTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testIndex()
    {
        // Crear un registro de laboratorio ficticio
        $registroId = 2;
        // Crear proyectos de ejemplo relacionados con el registro de laboratorio
        Proyecto::create([
            'registro_id' => $registroId,
            'nombre_proyecto' => 'Proyecto 1',
            'investigador_principal' => 'Investigador Principal 1',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI123',
            'resumen' => 'Resumen del Proyecto 1',
            'iba' => 'IBA123',
            'imagen_referencial' => 'images/proyecto1.jpg',
            'estado' => true,
        ]);
        Proyecto::create([
            'registro_id' => $registroId,
            'nombre_proyecto' => 'Proyecto 2',
            'investigador_principal' => 'Investigador Principal 2',
            'coinvestigadores' => 'Coinvestigador 3, Coinvestigador 4',
            'doi' => 'DOI456',
            'resumen' => 'Resumen del Proyecto 2',
            'iba' => 'IBA456',
            'imagen_referencial' => 'images/proyecto2.jpg',
            'estado' => true,
        ]);

        $response = $this->get("/api/operador/proyectos/$registroId");
        $response->assertStatus(200)
            ->assertJsonStructure(['proyectos']);
            //->assertJsonCount(2, 'proyectos');
    }

    public function testStore()
    {
        Storage::fake('public'); // Configura un sistema de archivos falso para almacenamiento

        $file = UploadedFile::fake()->image('test-image.jpg');

        $data = [
            'registro_id' => 2,
            'nombre_proyecto' => 'Nuevo Proyecto',
            'investigador_principal' => 'John Doe',
            'coinvestigadores' => 'Jane Smith, Mark Johnson',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_referencial' => $file,
            'estado' => true,
        ];

        $response = $this->post('/api/operador/proyectos/', $data);
        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);

        // Verifica que la imagen se haya almacenado correctamente
        //self::assertFileExists(storage_path('app/public/images/' . $file->hashName()));
        //Storage::disk('public')->assertFileExists('images/' . $file->hashName());
    }

    public function testShow()
    {
        $proyecto = Proyecto::create([
            'registro_id' => 2,
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_referencial' => 'images/proyecto.jpg',
        ]);

        $response = $this->get("/api/operador/proyectos/{$proyecto->proyecto_id}");
        $response->assertStatus(200)
            ->assertJsonStructure(['proyectos']);
    }

    public function testUpdate()
    {
        $proyecto = Proyecto::create([
            'registro_id' => 2,
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_referencial' => 'images/proyecto.jpg',
        ]);

        $data = [
            'registro_id' => 3,
            'nombre_proyecto' => 'Proyecto Actualizado',
            'investigador_principal' => 'Investigador Principal Actualizado',
            'coinvestigadores' => 'Coinvestigador 3, Coinvestigador 4',
            'doi' => 'DOI123',
            'resumen' => 'Resumen Actualizado',
            'iba' => 'IBA123',
            'imagen_referencial' => UploadedFile::fake()->image('updated-image.jpg'),
        ];

        $response = $this->put("/api/operador/proyectos/{$proyecto->proyecto_id}", $data);
        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
        $proyecto = Proyecto::create([
            'registro_id' => 2,
            'nombre_proyecto' => 'Proyecto de Prueba',
            'investigador_principal' => 'Investigador Principal de Prueba',
            'coinvestigadores' => 'Coinvestigador 1, Coinvestigador 2',
            'doi' => 'DOI789',
            'resumen' => 'Resumen de prueba',
            'iba' => 'IBA789',
            'imagen_referencial' => 'images/proyecto.jpg',
            'estado' => true,
        ]);

        $response = $this->put("/api/operador/proyectos/delete/{$proyecto->proyecto_id}");
        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);

        // Verifica que el estado se haya actualizado a falso
        $this->assertFalse(Proyecto::find($proyecto->proyecto_id)->estado);
    }
}
