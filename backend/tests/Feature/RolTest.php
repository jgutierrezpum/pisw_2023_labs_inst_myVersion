<?php

namespace Tests\Feature;

use App\Models\Rol;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RolTest extends TestCase
{
    use RefreshDatabase; // Para reiniciar la base de datos antes de cada prueba
    
    public function testIndex()
    {
        // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        // Insertar datos de prueba en la base de datos
        Rol::factory()->create(['tiporol' => 'Rol 1', 'estado' => true]);
        Rol::factory()->create(['tiporol' => 'Rol 2', 'estado' => true]);

        $response = $this->get('/api/roles');

        $response->assertStatus(200)
            ->assertJsonStructure(['roles']);
    }

    public function testStore()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $data = ['tiporol' => 'Nueva Rol'];

        $response = $this->post('/api/roles', $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Creado Correctamente']);
    }

    public function testShow()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $rol = Rol::factory()->create(['tiporol' => 'Rol de prueba', 'estado' => true]);

        $response = $this->get("/api/roles/{$rol->idrol}");

        $response->assertStatus(200)
            ->assertJson(['rol' => ['tiporol' => 'Rol de prueba']]);
    }

    public function testUpdate()
    {

         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();

        $rol = Rol::factory()->create(['tiporol' => 'Rol de prueba 2', 'estado' => true]);
        $data = ['tiporol' => 'Rol actualizada'];

        $response = $this->put("/api/roles/{$rol->idrol}", $data);

        $response->assertStatus(200)
            ->assertJson(['status' => 'Actualizado Correctamente']);
    }

    public function testDelete()
    {
         // Deshabilitar todos los middleware de autenticación
        $this->withoutMiddleware();
        
        $rol = Rol::factory()->create(['tiporol' => 'Rol a eliminar', 'estado' => true]);

        $response = $this->delete("/api/roles/{$rol->idrol}");

        $response->assertStatus(200)
            ->assertJson(['status' => 'Eliminado Correctamente']);
    }
}
